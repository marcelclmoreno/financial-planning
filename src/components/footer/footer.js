import React, { Component } from 'react'
import './footer.css'

class Footer extends Component {
	render() {
		return (
			<div className="footer" tabIndex="0">
				<div className="footer-inner parent-height valign-block-middle justify-block-center">
					<span className="bold">Teste de frontend</span>&nbsp;- Marcel Costa Leão Moreno
				</div>
			</div>
		)
	}
}

export default Footer