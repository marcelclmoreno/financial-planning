import React, { Component } from 'react'
import './input.css'

class Input extends Component {
	static defaultProps = {
		inputTypes: {
			money: {
				type: 'text',
				placeholder: '1234',
				label: 'Valor:',
				errorMessage: 'Valor inválido!'
			},
			description: {
				type: 'text',
				placeholder: 'Texto de entrada',
				label: 'Descrição:',
				errorMessage: 'Descrição inválida!'
			}
		},
		validateField() {
			let isValid = false
			let value = document.getElementById(this.id).value

			switch (this.type) {
				case 'money':
					let regexNumber = /^\d+$/
					isValid = regexNumber.test(value)
					break

				case 'description':
				default:
					isValid = value ? true : false
					break
			}

			let error = document.getElementById(this.id + '-error')

			if (!isValid) {
				error.style = 'display: block'
			} else {
				error.style = 'display: none'
			}

			return isValid
		},
		value(value) {
			let input = document.getElementById(this.id)

			if (value !== undefined) {
				return input.value = value
			} else {
				return input.value
			}
		}
	}
	render() {
		return (
			<div className="input" tabIndex="0">
				<label htmlFor={this.props.id}>
					{this.props.inputTypes[this.props.type].label}
				</label>

				<input id={this.props.id}
					type={this.props.inputTypes[this.props.type].type}
					name={this.props.name}
					placeholder={this.props.inputTypes[this.props.type].placeholder}
					ref={this}
				/>

				<div id={this.props.id + '-error'} className="error align-center bold" style={{ display: 'none' }}>
					{this.props.inputTypes[this.props.type].errorMessage}
				</div>
			</div>
		)
	}
}

export default Input