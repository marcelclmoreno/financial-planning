import React, { Component } from 'react'
import './button.css'

class Button extends Component {
	clickHandler() {
		this.props.click()
	}
	render() {
		return (
			<div className="button" tabIndex="0">
				<button type="submit" onClick={this.clickHandler.bind(this)}>
					{this.props.text}
				</button>
			</div>
		)
	}
}

export default Button