import React, { Component } from 'react'
import './table.css'
import Button from "./../button/button.js"

class Table extends Component {
	render() {
		let that = this

		function handleDeleteClick(key) {
			that.props.delete(key)
		}
		
		let expenses = undefined

		if (this.props.data.length > 0) {
			expenses = this.props.data.map(function (expense) {
				return (<div className="row display-flex" key={expense.key}>
					<div className="data valign-block-middle column-count2">
						{expense.description}
					</div>
					<div className="data valign-block-middle column-count1">
						{expense.value}
					</div>
					<div className="data valign-block-middle column-count1">
						<Button text="Apagar" click={handleDeleteClick.bind(this, expense.key)} />
					</div>
				</div>)
			})
		} else {
			expenses = (
				<div className="empty-state align-center">Nenhum gasto cadastrado</div>
			)
		}

		return (
			<div className="table" tabIndex="0">
				<div className="table-header">
					<div className="row display-flex">
						<div className="data display-flex bold column-count2">
							Descrição
						</div>
						<div className="data display-flex bold column-count1">
							Valor
						</div>
						<div className="data display-flex bold column-count1">
						</div>
					</div>
				</div>
				<div className="table-body">
					{expenses}
				</div>
			</div>
		)
	}
}

export default Table