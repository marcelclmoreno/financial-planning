import React, { Component } from 'react'
import './header.css'

class Header extends Component {
	render() {
		return (
			<div className="header shadow2" tabIndex="0">
				<div className="header-inner parent-height valign-block-middle">
					<img src="logo.png" alt="Planejamento Financeiro" className="display-block" title="Planejamento Financeiro" aria-hidden />
					Planejamento Financeiro
				</div>
			</div>
		)
	}
}

export default Header