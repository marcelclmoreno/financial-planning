import React, { Component } from 'react'
import Header from './components/header/header.js'
import Footer from './components/footer/footer.js'
import './app.css'
import Input from './components/input/input.js'
import Button from './components/button/button.js'
import Table from './components/table/table.js'
import uuid from 'uuid'

class App extends Component {
	constructor() {
		super()
		this.state = {
			total: 0,
			expenses: []
		}
	}
	submitHandler() {
		var isValid = true

		if (!this.descriptionInput.props.validateField()) {
			isValid = false
		}
		if (!this.valueInput.props.validateField()) {
			isValid = false
		}

		if (isValid) {
			let expenses = this.state.expenses
			expenses.push({
				key: uuid.v4(),
				description: this.descriptionInput.props.value(),
				value: this.valueInput.props.value()
			})
			this.setState({ expenses: expenses })

			this.descriptionInput.props.value('')
			this.valueInput.props.value('')
			this.calcTotal() 
		}
	}
	deleteHandler(key) {
		let expenses = this.state.expenses
		let index = expenses.findIndex(elem => elem.key === key)
		expenses.splice(index, 1)
		this.setState({ expenses: expenses })
		this.calcTotal() 
	}
	calcTotal() { 
		let expenses = this.state.expenses
		let newTotal = 0
		expenses.forEach(function(expense) {
			newTotal = Number(newTotal) + Number(expense.value)
		})
		this.setState({ total: newTotal })
	}
	render() {
		return (
			<div className="app display-flex flex-column justify-block-between">
				<Header />

				<div className="app-inner center-block ">
					<div className="total display-flex valign-block-middle justify-block-center" tabIndex="0">
						<span className="bold">Gasto total:&nbsp;</span> <span className="value">{this.state.total}</span>
					</div>

					<div className="card">
						<div className="title bold">
							Cadastrar novo gasto:
						</div>

						<div className="display-flex flex-form">
							<div className="display-flex flex-grow1 form-spacing">
								<Input id="registerDescription" type="description" name="description" ref={el => this.descriptionInput = el} />
							</div>
							<div className="display-flex flex-grow1 form-spacing">
								<Input id="registerValue" type="money" name="value" ref={el => this.valueInput = el} />
							</div>
							<div className="display-flex flex-grow1 form-spacing">
								<Button text="Cadastrar Gasto" click={this.submitHandler.bind(this)} />
							</div>
						</div>
					</div>

					<Table data={this.state.expenses} delete={this.deleteHandler.bind(this)} />

				</div>

				<Footer />
			</div>
		)
	}
}

export default App