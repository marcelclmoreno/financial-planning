import React from 'react'
import ReactDOM from 'react-dom'
import './static/maxi.css'
import './static/resets.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'

ReactDOM.render(<App />, document.getElementById('root'))
registerServiceWorker()